<?php

return [
    'table' => [
        'cats' => [
        	'rownumber' => '#',
	        'title' => 'Caption',
	        'content' => 'Description',
	        'date' => 'Date',
	        'oprations' => '&nbsp;'
        ],
        'tags' => [
        	'rownumber' => '#',
	        'title' => 'Caption',
	        'content' => 'Description',
	        'date' => 'Date',
	        'oprations' => '&nbsp;'
        ]
    ],
    'forms' => [
        'new' => [
        	'main_box_title' => 'New Seo',
        	'textbox_title' => 'Caption',
	        'textbox_content' => 'Explain more....',
	        'save_box_title' => 'Save',
	        'btn_save_cat' => 'Save as Category',
	        'btn_save_tag' => 'Save as Tag',
        ],
        'edit' => [
            'main_box_title' => 'Edit :seoType : <small> at <mark>:create_time</mark> created and last Update at <mark>:up_time</mark></small>',
            'save_box_title' => 'Save',
            'btn_save_cat' => 'Save Change',
            'btn_save_tag' => 'Save Change',
        ]
    ],


];