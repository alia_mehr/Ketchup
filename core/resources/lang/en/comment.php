<?php

return [
    'table' => [
    	'rownumber' => '#',
        'name' => 'Name',
        'email' => 'Email',
        'content' => 'Message',
        'date' => 'Date',
        'status' => 'Status',
        'oprations' => '&nbsp;'
    ],
    'controll' => [
        'status' => [
            '0' => 'UnRead',
            '1' => 'Confirmed',
            '2' => 'Unverified',
            '3' => 'New Reply',
            '4' => 'To Review',
            '5' => 'Spam'
        ],
    ],
    'forms' => [
        'new' => [
        	'main_box_title' => 'New Comment',
            'textbox_title' => 'Comment Title',
        	'textbox_url' => 'Comment Url',
            'textbox_content' => 'Explain more....',
	        'textbox_tags' => 'Tags',
            'comment_status_list_title' => 'I accept Comments :',
            'comment_status_list' => [
                '0' => 'anyone',
                '1' => 'only members',
                '2' => 'Nobody',
                '3' => '',
                '4' => '',
                '5' => '',
                '6' => '',
                '7' => '',
                '8' => '',
                '9' => ''
            ]
        ],
        'edit' => [
            'main_box_title' => 'Edit Comment ',
            'name' => 'Name',
            'email' => 'Email',
            'contentComment' => 'Comment',
            'replyContent' => 'Reply',
            'save_box_title' => 'Save',
            'replies_box_title' => 'Repleis',
            'details_box_title' => 'Details',
            'details_created' => '<strong>Date of Sending :</strong><br> ',
            'details_updated' => '<strong>Date of Change :</strong><br> ',
            'details_post' => '<strong>For This Post :</strong> <a href=":link">Link</a>',
            'details_parent' => '<strong>Answer to :</strong> <a href=":link">Link</a>',
            'details_user' => '<strong>Information Sender :</strong> <a href=":link">Link</a>',
            'status_list_title' => 'This Comment',
            'status_list' => [
                '0' => '',
                '1' => 'Confirmed',
                '2' => 'Unverified',
                '3' => '',
                '4' => 'To Review',
                '5' => 'Spam',
                '6' => '',
                '7' => '',
                '8' => '',
                '9' => ''
            ],
            'reply_count' => 'There are <span class="badge">:count</span> answer(s) to this comment',
            'reply_new' => '<span class="badge">New Reply received</span>',
            'btn_replies' => 'Review Repleis',
            'btn_send' => 'Save'
        ]
    ],


];