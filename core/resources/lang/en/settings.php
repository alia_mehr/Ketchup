<?php

return [
    'form' => [
    	'panel_box' => [
            'box_title' => 'Panel',
            'title' => 'Title',
            'notic' => 'Notic'
        ],
        'site_box' => [
            'box_title' => 'Site',
            'title' => 'Title',
            'name' => 'Name',
            'description' => 'Description',
            'tags' => 'Tags',
            'availability' => [
                'label' => 'Availability',
                'forall' => 'For all',
                'users' => 'Only users',
                'nobody' => 'Nobody',
            ],
            'no_posts' => 'Total posts in the Home',
            'abstracts' => 'The number of characters Abstracts posts',
            'footer' => 'Footer',
            'rtl' => 'Right to Left',
            'notic' => 'Notic'
        ],
        'auth_box' => [
            'box_title' => 'Auth',
            'register' => 'Register',
            'author' => 'Author Page'
        ],
        'file_box' => [
            'box_title' => 'File',
            'mime' => 'MIME',
            'max_size' => [
                'label' => 'Max upload size',
                'in' => 'Kilobytes (1024 Kb = 1 Mb)'
            ]
        ],
        'save_box' => [
            'box_title' => 'Save Settings',
            'btn_send' => 'Save'
        ],
        'input_option' => [
            'yes' => 'Yes',
            'no' => 'No'
        ]
    ],


];