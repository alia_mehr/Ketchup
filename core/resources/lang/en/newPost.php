<?php

return [
    'post_box' => [
        'title' => 'New Post',
        'edit_title' => 'This Post :<small> at <mark>:create_time</mark> created and last Update at <mark>:up_time</mark></small>',
        'textbox_title' => 'What do you write about?',
        'textbox_content' => 'Explain more....',
        'textbox_tags' => 'Select Tags'
    ],
    'save_box' => [
        'title' => 'Save',
        'btn_send' => 'Save',
        'status_list_title' => 'This Post:',
        'status_list' => [
            '0' => 'Publish',
            '1' => 'Special Members',
            '2' => 'Draft',
            '3' => 'For Review',
            '4' => 'Hide at Home',
            '5' => '',
            '6' => '',
            '7' => '',
            '8' => '',
            '9' => ''
        ],
        'comment_status_list_title' => 'I accept Comments :',
        'comment_status_list' => [
            '0' => 'anyone',
            '1' => 'only members',
            '2' => 'Nobody',
            '3' => '',
            '4' => '',
            '5' => '',
            '6' => '',
            '7' => '',
            '8' => '',
            '9' => ''
        ]
    ],
    'category_box' => [
        'title' => 'Categories'
    ],

];