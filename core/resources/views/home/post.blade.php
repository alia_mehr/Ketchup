@extends('layouts.home')

@section('title',"$init->title | $post->title")
@section('description',substr($post->body,0,$init->abstracts))
@section('tags')
    @foreach($post->tagName as $tag) 
        {{ $tag . ',' }}
    @endforeach
@endsection

@section('author',"$post->AuthorName")

@section('siteName',"$init->name")
@section('siteDesc',"$init->description")
@section('footer',"$init->footer")
@section('links')
    @foreach($links as $link)
        <li class="active"><a href="{{ $link->address }}" title="{{ $link->description }}">{{ $link->title }}</a></li>
    @endforeach
@endsection
@section('content')
    
    <small>
        <h2><a href="{{ $post->url or url('/post/'.$post->id) }}" style="text-decoration:none;">{{ $post->title }}</a></h2>
    </small>
    <hr>
    <?php $colorsArr[0] = 'success';$colorsArr[1] = 'info';$colorsArr[2] = 'warning';$colorsArr[3] = 'danger'; ?>
    
    <h5><span class="glyphicon glyphicon-user"></span> &shy;<a href="{{ url('author/'.$post->user->name) }}">{{ $post->user->name }}</a> &shy;  <span class="glyphicon glyphicon-time"></span> &shy;<a href="{{ url('/filter/?date='.$post->updated_at) }}">{{ $post->updated_at }}</a> &shy;  <span class="glyphicon glyphicon-folder-open"></span> &shy; 
    @foreach($post->catNames as $cat) 
        <a href="{{ url('/filter/?category='.$cat) }}">{{ $cat }}</a>,
    @endforeach
    </h5>
    <h5></h5>
    {{ $post->body }}
    <h5>
    
    @foreach($post->tagName as $key => $tag) 
        <span class="label label-{{ $colorsArr[$key%4] }}"><a href="{{ url('/filter/?tag='.$tag) }}">{{ $tag }}</a></span>
    @endforeach
    <br></h5>
    <hr>

    <h4 id="comment">{{ $commFlag or trans('home.index.comment_form') }}</h4>
    @if($post->comments_status < 2 )
    @if(!isset($user) && ($post->comments_status == 1))
        @lang('home.errors.comment.only_user')
    @else
    <form role="form" method="POST" action="">
        {{ csrf_field() }}

        <div class="form-group{{ $errors->has('commName') ? ' has-error' : '' }}">
            <div class="input-group">
                <div class="input-group-addon">@lang('home.forms.comment.textbox_name')</div>
                <input id="commName" type="text" class="form-control" name="commName" value="{{ $user->information->name or old('commName') }}">
            </div>
            @if ($errors->has('commName'))
                <span class="help-block">
                <strong>{{ $errors->first('commName') }}</strong>
                </span>
            @endif
        </div>

        <div class="form-group{{ $errors->has('commEmail') ? ' has-error' : '' }}">
            <div class="input-group">
                <div class="input-group-addon">@lang('home.forms.comment.textbox_email')</div>
                <input id="commEmail" type="text" class="form-control" name="commEmail" value="{{ $user->information->email or old('commEmail') }}">
            </div>
            @if ($errors->has('commEmail'))
                <span class="help-block">
                <strong>{{ $errors->first('commEmail') }}</strong>
                </span>
            @endif
        </div>

        <div class="form-group{{ $errors->has('commBody') ? ' has-error' : '' }}">
            <div class="input-group">
                <div class="input-group-addon">@lang('home.forms.comment.textbox_body')</div>
                <textarea class="form-control" rows="4" id="commBody" name="commBody">{{ old('commBody') }}</textarea>
            </div>
            @if ($errors->has('commBody'))
                <span class="help-block">
                <strong>{{ $errors->first('commBody') }}</strong>
                </span>
            @endif
        </div>

        <button type="submit" class="btn btn-success">@lang('home.forms.comment.button_send')</button>
    </form>
    @endif
    @else
        @lang('home.errors.comment.nobody')
    @endif
    <br><br>

    <p><span class="badge">{{ count($comms) }}</span> @lang('home.index.comment_list')</p><br>

    
    <?php $father = '';$i=0; ?>
<div class="row">
    @foreach($comms as $key => $c)
    <?php $family = explode('b',$key)?>
       @if( count($family)< 2 ) </div><div class="row">
            <div class="col-sm-10">
                <h4>{{ $c->name }} <small>{{ $c->created_at }}</small></h4>
                <p>{{ $c->body }}<a class="pull-right btn-link" href="?cmid={{ $c->id }}#comment">@lang('home.index.comment_reply')</a></p> 
                <br>
            </div>
            <?php $father = $family[1];$i++; ?>
        @elseif($father == $family[1])
                <div class="col-xs-{{ (12-$i) }} col-xs-offset-{{ $i }}">
                    <h4>{{ $c->name }} <small>{{ $c->created_at }}</small></h4>
                    <p>{{ $c->body }}<a class="pull-right btn-link" href="?cmid={{ $c->id }}#comment">@lang('home.index.comment_reply')</a></p>
                    <br>
                </div>
        @elseif($father != $family[1])
            <?php $father = $family[1];$i++; ?>
                <div class="col-xs-{{ (12-$i) }} col-xs-offset-{{ $i }}">
                    <h4>{{ $c->name }} <small>{{ $c->created_at }}</small></h4>
                    <p>{{ $c->body }}<a class="pull-right btn-link" href="?cmid={{ $c->id }}#comment">@lang('home.index.comment_reply')</a></p>
                    <br>
                </div>
        @endif
    @endforeach
        </div>
    
    <br><br>
@endsection
