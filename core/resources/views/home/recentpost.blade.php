@extends('layouts.home')

@section('title',"$init->title")
@section('description',"$init->description")
@section('tags',"$init->tags")
@section('author',"$init->SuperAdmin")

@section('siteName',"$init->name")
@section('siteDesc',"$init->description")
@section('footer',"$init->footer")

@section('links')
    @foreach($links as $link)
        <li class="active"><a href="{{ $link->address }}" title="{{ $link->description }}">{{ $link->title }}</a></li>
    @endforeach
@endsection

@section('content')
    <h4><small>{{ $init->site_notic or trans('home.index.notic_place') }}</small></h4>
    <hr>
    <?php $colorsArr[0] = 'success';$colorsArr[1] = 'info';$colorsArr[2] = 'warning';$colorsArr[3] = 'danger'; ?>
    @foreach($posts as $post)
        <h2><a href="{{ $post->url or url('/post/'.$post->id) }}" style="text-decoration:none;">{{ $post->title }}</a></h2>
        <h5><span class="glyphicon glyphicon-user"></span> &shy;<a href="{{ url('/author/'.$post->user->name) }}">{{ $post->user->name }}</a> &shy;  <span class="glyphicon glyphicon-time"></span> &shy;<a href="{{ url('/filter/?date='.$post->updated_at) }}">{{ $post->updated_at }}</a> &shy;  <span class="glyphicon glyphicon-folder-open"></span> &shy; 
        @foreach($post->catNames as $cat) 
            <a href="{{ url('/filter/?category='.$cat) }}">{{ $cat }}</a>,
        @endforeach
        </h5>
        <h5></h5>
        {{ substr($post->body,0,$init->abstracts) }}
        <h5>
        
        @foreach($post->tagName as $key => $tag) 
            <span class="label label-{{ $colorsArr[$key%4] }}"><a href="{{ url('/filter/?tag='.$tag) }}">{{ $tag }}</a></span>
        @endforeach
        <br><br></h5>
    @endforeach
        <br><br>
    @if(method_exists($posts,'links'))
        {{ $posts->links() }}
    @endif
@endsection