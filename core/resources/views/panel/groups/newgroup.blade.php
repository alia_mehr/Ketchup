@extends('layouts.panel')

@section('sidbaractive_groups','current')
@section('sidbaractive_newgroups','current')
@section('content_panel')
<link href="/build/tags/css/bootstrap-tags.css" rel="stylesheet">

<div class="col-md-10">
    <div class="row">
    <form class="form-horizontal" role="form" method="POST" action="{{ url('/panel/groups/new') }}">
        <div class="col-md-5">
            <div class="content-box-header">
                <div class="panel-title">@lang('group.forms.new.user_box_title')</div>

                <div class="panel-options">
                    <a href="#" data-rel="collapse"><i class="glyphicon glyphicon-repeat"></i></a>
                </div>
            </div>
            <div class="content-box-large box-with-header">
                
                @foreach ($fieldsRules['user'] as $key => $ruleText)
                    <h4>
                        <span class="label label-default">{{ $ruleText }}</span>
                        <div class="btn-group pull-right" data-toggle="buttons">
                            <label class="btn btn-default btn-xs active">
                                <input type="radio" name="userRule[{{ $key }}]" id="option{{ $key }}" value="1" checked> @lang('group.forms.new.btn_allow')
                            </label>
                            <label class="btn btn-default btn-xs">
                                <input type="radio" name="userRule[{{ $key }}]" id="option{{ $key }}" value="0"> @lang('group.forms.new.btn_ban')
                            </label>
                        </div>
                    </h4>
                @endforeach
                
            </div>
        </div>
        <div class="col-md-5">
            <div class="content-box-header">
                <div class="panel-title">@lang('group.forms.new.manage_box_title')</div>

                <div class="panel-options">
                    <a href="#" data-rel="collapse"><i class="glyphicon glyphicon-repeat"></i></a>
                </div>
            </div>
            <div class="content-box-large box-with-header">

                @foreach ($fieldsRules['manage'] as $key => $ruleText)
                    <h4>
                        <span class="label label-default">{{ $ruleText }}</span>
                        <div class="btn-group pull-right" data-toggle="buttons">
                            <label class="btn btn-default btn-xs">
                                <input type="radio" name="manageRule[{{ $key }}]" id="option{{ $key }}" value="1"> @lang('group.forms.new.btn_allow')
                            </label>
                            <label class="btn btn-default btn-xs active">
                                <input type="radio" name="manageRule[{{ $key }}]" id="option{{ $key }}" value="0" checked > @lang('group.forms.new.btn_ban')
                            </label>
                        </div>
                    </h4>
                @endforeach
            </div>
        </div>

        <div class="col-md-2">
            <div class="content-box-header">
                <div class="panel-title">@lang('group.forms.new.save_box_title')</div>
            </div>
            <div class="content-box-large box-with-header">
                <div class="form-group{{ $errors->has('groupTitle') ? ' has-error' : '' }}">
                    <input id="groupTitle" type="text" class="form-control" name="groupTitle" placeholder="@lang('group.forms.new.name_group')">
                    @if ($errors->has('groupTitle'))
                        <span class="help-block">
                        <strong>{{ $errors->first('groupTitle') }}</strong>
                        </span>
                    @endif
                </div>
                {{ csrf_field() }}
                <button type="submit" class="btn btn-success btn-lg btn-block" name="opration" value="save"><i class="glyphicon glyphicon-saved"></i> @lang('page.forms.new.btn_send') </button>
                
            </div>
        </div>
    </form>

    </div>
</div>

@endsection