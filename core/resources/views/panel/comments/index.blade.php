@extends('layouts.panel')

@section('sidbaractive_comment','current')
@if($listType == 'spam')
    @section('sidbaractive_spamcomment','current')
@elseif($listType != 'replies')
    @section('sidbaractive_allcomment','current')
@endif
@section('content_panel')
<div class="col-md-10">
    <div class="row">
        <div class="col-md-12">
            <div class="content-box-large">
                <div class="panel-heading">
                    <div class="panel-options">
                    <?php if(($listType == 'lump')||($listType == 'mote')){ ?>
                        <div class="btn-group btn-group-xs" role="group" aria-label="...">
                          <a class="btn btn-default<?= ($listType == 'lump') ? '-selected': ''; ?>" href="{{ url('/panel/comments/') }}<?='/'.$from?> "><i class="glyphicon glyphicon-th-list"></i></a>
                          <a class="btn btn-default<?= ($listType == 'mote') ? '-selected': ''; ?>" href="{{ url('/panel/comments') }}<?='/'.$from.'/mote'?>"><i class="glyphicon glyphicon-list"></i></a>
                        </div>
                    <?php } ?>
                    &shy;
                    <?php if (isset($from)) { ?>
                      <div class="btn-group btn-group-xs" role="group" aria-label="...">
                        <a class="btn btn-default<?= ($from == 'me') ? '-selected': ''; ?>" href="{{ url('/panel/comments/me') }}<?='/'.$listType ?>">@lang('panel.adminOption.me')</a>
                        <a class="btn btn-default<?= ($from == 'all') ? '-selected': ''; ?>" href="{{ url('/panel/comments/all') }}<?='/'.$listType ?>">@lang('panel.adminOption.all')</a>
                      </div>
                    <?php } ?>
                    </div>

                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                  <th>@lang('comment.table.rownumber')</th>
                                  <th>@lang('comment.table.name')</th>
                                  <th>@lang('comment.table.email')</th>
                                  <th>@lang('comment.table.content')</th>
                                  <th>@lang('comment.table.status')</th>
                                  <th>@lang('comment.table.date')</th>
                                  <th>@lang('comment.table.oprations')</th>
                              </tr>
                          </thead>
                          <tbody>
                            <?php $i=0 ?>
                            @foreach($allComm as $key => $comment)
                            <tr>
                              <td>{{ ++$i }}</td>
                              <td>{{ substr($comment['name'],0,20) }}</td>
                              <td>{{ substr($comment['email'],0,60) }}</td>
                              <td>{{ substr($comment['body'],0,60) }}</td>
                              <td>{{ $comment['status'] }}</td>
                              <td>{{ $comment['updated_at'] }}</td>
                              <td><a href="{{ url('/p') .'/'. $comment['post_id'] }}"><i class="glyphicon glyphicon-eye-open"></i></a></td>
                              <td><a href="{{ url('/panel/comments/reply') .'/'. $comment['id'] }}"><i class="glyphicon glyphicon-edit"></i></a></td>
                              <td><a href="{{ url('/panel/comments/delete') .'/'. $comment['id'] }}"><i class="glyphicon glyphicon-trash"></i></a></td>
                          </tr>
                          @endforeach
                      </tbody>
                  </table>
              </div>
          </div>
      </div>
  </div>
</div>
</div>
@endsection