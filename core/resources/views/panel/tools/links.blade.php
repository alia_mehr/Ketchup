@extends('layouts.panel')

@section('sidbaractive_tool','current')
@section('sidbaractive_links','current')
@section('content_panel')
<div class="col-md-10">
    <div class="row">
        <div class="col-md-12">
            <div class="content-box-large">
                <div class="panel-heading">
                    <div class="panel-title">@lang('tools.links.sections.add')</div>
                    <div class="panel-options">
                
                    </div>
                </div>
                <div class="panel-body">
                  <form name="addNew" class="form-horizontal" role="form" method="POST" action="">
                    {{ csrf_field() }}
                    <input type="hidden" name="linkId" value="{{ $editUrl->id or '' }}">
                    <div class="row">
                      <div class="col-md-5">
                        <div class="form-group{{ $errors->has('linkTitle') ? ' has-error' : '' }}">
                            <input id="linkTitle" type="text" class="form-control" name="linkTitle" placeholder="@lang('tools.links.form.title')" value="{{ $editUrl->title or old('linkTitle') }}">
                            @if ($errors->has('linkTitle'))
                                <span class="help-block">
                                <strong>{{ $errors->first('linkTitle') }}</strong>
                                </span>
                            @endif
                        </div>
                      </div>
                      <div class="col-md-6 col-md-offset-1">

                        <div class="form-group{{ $errors->has('linkAddress') ? ' has-error' : '' }}">
                            <input id="linkAddress" type="url" class="form-control" name="linkAddress" placeholder="@lang('tools.links.form.address')" value="{{ $editUrl->address or old('linkAddress') }}">
                            @if ($errors->has('linkAddress'))
                                <span class="help-block">
                                <strong>{{ $errors->first('linkAddress') }}</strong>
                                </span>
                            @endif
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-8">
                        <div class="form-group{{ $errors->has('linkDescription') ? ' has-error' : '' }}">
                            <textarea id="linkDescription" row="3" class="form-control" name="linkDescription" placeholder="@lang('tools.links.form.description')">{{ $editUrl->description or old('description') }}</textarea>
                            @if ($errors->has('linkDescription'))
                                <span class="help-block">
                                <strong>{{ $errors->first('linkDescription') }}</strong>
                                </span>
                            @endif
                        </div>
                      </div>
                      <div class="col-md-4">
                        <button type="submit" class="btn btn-success btn-lg btn-block" name="opration" value="add"><i class="glyphicon glyphicon-saved"></i> @lang('tools.links.form.save') </button>
                      </div>
                    </div>
                  </form>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="content-box-large">
                <div class="panel-heading">
                    <div class="panel-title">@lang('tools.links.sections.list')</div>
                    
                    <div class="panel-options">
                    

                    </div>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                  <th>@lang('tools.links.tableColumns.rownumber')</th>
                                  <th>@lang('tools.links.tableColumns.title')</th>
                                  <th>@lang('tools.links.tableColumns.content')</th>
                                  <th>@lang('tools.links.tableColumns.address')</th>
                                  <th>@lang('tools.links.tableColumns.date')</th>
                                  <th>@lang('tools.links.tableColumns.oprations')</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($allLinks as $key => $link)
                                <tr>
                                  <td>{{ $key+1 }}</td>
                                  <td>{{ substr($link->title,0,20) }}</td>
                                  <td>{{ strip_tags(substr($link->description,0,60)) }}</td>
                                  <td><a href="{{ $link->address }}">{{ strip_tags(substr($link->address,0,30)) }}</a></td>
                                  <td>{{ $link->updated_at }}</td>
                                  <form name="oprationBtns" class="form-horizontal" role="form" method="POST" action="">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="linkId" value="{{ $link->id }}">
                                    <td><button type="submit" class="btn btn-link btn-xs" name="opration" value="display"><i class="glyphicon glyphicon-edit"></i></button></td>
                                    <td><button type="submit" class="btn btn-link btn-xs" name="opration" value="delete"><i class="glyphicon glyphicon-remove"></i></button></td>
                                  </form>

                                  
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection