@extends('layouts.panel')

@section('sidbaractive_post','current')
@section('sidbaractive_trashpost','current')
@section('content_panel')
<div class="col-md-10">
    <div class="row">
        <div class="col-md-12">
            <div class="content-box-large">
                <div class="panel-heading">
                    <div class="panel-title"></div>
                    
                    <div class="panel-options">
                    <?php if (isset($from)) { ?>
                      <div class="btn-group btn-group-xs" role="group" aria-label="...">
                        <a class="btn btn-default<?= ($from == 'me') ? '-selected': ''; ?>" href="{{ url('/panel/posts/trash') }}">@lang('panel.adminOption.me')</a>
                        <a class="btn btn-default<?= ($from == 'all') ? '-selected': ''; ?>" href="{{ url('/panel/posts/trash/all') }}">@lang('panel.adminOption.all')</a>
                      </div>
                    <?php } ?>

                    </div>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                  <th>@lang('listPosts.tableColumns.rownumber')</th>
                                  <th>@lang('listPosts.tableColumns.title')</th>
                                  <th>@lang('listPosts.tableColumns.content')</th>
                                  <th>@lang('listPosts.tableColumns.date')</th>
                                  <th>@lang('listPosts.tableColumns.oprations')</th>
                              </tr>
                          </thead>
                          <tbody>
                            @foreach($allPosts as $key => $post)
                            <tr>
                              <td>{{ $key+1 }}</td>
                              <td>{{ substr($post['title'],0,20) }}</td>
                              <td>{{ substr($post['body'],0,60) }}</td>
                              <td>{{ $post['updated_at'] }}</td>
                                <td><a href="{{ url('/panel/posts/trash/restore') .'/'. $post['id'] }}"><i class="glyphicon glyphicon-share-alt"></i></a></td>
                                <td><a href="{{ url('/panel/posts/trash/remove') .'/'. $post['id'] }}"><i class="glyphicon glyphicon-remove"></i></a></td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
</div>
</div>
@endsection