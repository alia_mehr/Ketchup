@extends('layouts.panel')

@section('sidbaractive_post','current')
@section('sidbaractive_allpost','current')
@section('content_panel')
<div class="col-md-10">
    <div class="row">
        <div class="col-md-12">
            <div class="content-box-large">
                <div class="panel-heading">
                    <div class="panel-title"></div>
                    
                    <div class="panel-options">
                    <?php if (isset($from)) { ?>
                      <div class="btn-group btn-group-xs" role="group" aria-label="...">
                        <a class="btn btn-default<?= ($from == 'me') ? '-selected': ''; ?>" href="{{ url('/panel/posts') }}">@lang('panel.adminOption.me')</a>
                        <a class="btn btn-default<?= ($from == 'all') ? '-selected': ''; ?>" href="{{ url('/panel/posts/all') }}">@lang('panel.adminOption.all')</a>
                      </div>
                    <?php } ?>

                    </div>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                  <th>@lang('listPosts.tableColumns.rownumber')</th>
                                  <th>@lang('listPosts.tableColumns.title')</th>
                                  <th>@lang('listPosts.tableColumns.content')</th>
                                  <th>@lang('listPosts.tableColumns.date')</th>
                                  <th>@lang('listPosts.tableColumns.oprations')</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($allPosts as $key => $post)
                                <tr>
                                  <td>{{ $key+1 }}</td>
                                  <td>{{ substr($post['title'],0,20) }}</td>
                                  <td>{{ strip_tags(substr($post['body'],0,60)) }}</td>
                                  <td>{{ $post['updated_at'] }}</td>
                                  @if($from == 'me')
                                  @if(session('uRule')[1])<td><a href="{{ url('/panel/posts/edit') .'/'. $post['id'] }}"><i class="glyphicon glyphicon-edit"></i></a></td>@endif
                                  @if(session('uRule')[2])<td><a href="{{ url('/panel/posts/delete') .'/'. $post['id'] }}"><i class="glyphicon glyphicon-trash"></i></a></td>@endif
                                  @else
                                  @if(session('mRule')[0])<td><a href="{{ url('/panel/posts/edit') .'/'. $post['id'] }}"><i class="glyphicon glyphicon-edit"></i></a></td>@endif
                                  @if(session('mRule')[1])
                                  <td><a href="{{ url('/panel/posts/delete') .'/'. $post['id'] }}"><i class="glyphicon glyphicon-trash"></i></a></td>@endif
                                  @endif
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection