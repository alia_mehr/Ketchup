@extends('layouts.panel')

@section('sidbaractive_page','current')
@section('sidbaractive_allpage','current')
@section('content_panel')
<div class="col-md-10">
    <div class="row">
        <div class="col-md-12">
            <div class="content-box-large">
                <div class="panel-heading">
                    <div class="panel-title"></div>
                    
                    <div class="panel-options">
                    <?php if (isset($from)) { ?>
                      <div class="btn-group btn-group-xs" role="group" aria-label="...">
                        <a class="btn btn-default<?= ($from == 'me') ? '-selected': ''; ?>" href="{{ url('/panel/pages') }}">@lang('panel.adminOption.me')</a>
                        <a class="btn btn-default<?= ($from == 'all') ? '-selected': ''; ?>" href="{{ url('/panel/pages/all') }}">@lang('panel.adminOption.all')</a>
                      </div>
                    <?php } ?>

                    </div>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                  <th>@lang('page.table.rownumber')</th>
                                  <th>@lang('page.table.title')</th>
                                  <th>@lang('page.table.content')</th>
                                  <th>@lang('page.table.date')</th>
                                  <th>@lang('page.table.oprations')</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($allPages as $key => $page)
                                <tr>
                                  <td>{{ $key+1 }}</td>
                                  <td>{{ substr($page['title'],0,20) }}</td>
                                  <td>{{ strip_tags(substr($page['body'],0,60)) }}</td>
                                  <td>{{ $page['updated_at'] }}</td>
                                  @if($from == 'me')
                                  @if(session('uRule')[5])<td><a href="{{ url('/panel/pages/edit') .'/'. $page['id'] }}"><i class="glyphicon glyphicon-edit"></i></a></td>@endif
                                  @if(session('uRule')[6])<td><a href="{{ url('/panel/pages/delete') .'/'. $page['id'] }}"><i class="glyphicon glyphicon-trash"></i></a></td>@endif
                                  @else
                                  @if(session('mRule')[3])<td><a href="{{ url('/panel/pages/edit') .'/'. $page['id'] }}"><i class="glyphicon glyphicon-edit"></i></a></td>@endif
                                  @if(session('mRule')[4])<td><a href="{{ url('/panel/pages/delete') .'/'. $page['id'] }}"><i class="glyphicon glyphicon-trash"></i></a></td>@endif
                                  @endif

                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection