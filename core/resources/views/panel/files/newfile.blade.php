@extends('layouts.panel')

@section('sidbaractive_file','current')
@section('sidbaractive_newfile','current')
@section('content_panel')
<?php
// dd($errors); 
?>
<link href="/build/tags/css/bootstrap-tags.css" rel="stylesheet">

<div class="col-md-10">
    <div class="row">
    <form class="form-horizontal" role="form" method="POST" action="" enctype="multipart/form-data">
        <div class="col-md-9">
            <div class="content-box-header">
                <div class="panel-title">@lang('file.forms.new.main_box_title')</div>

                <div class="panel-options">
                    <a href="#" data-rel="collapse"><i class="glyphicon glyphicon-repeat"></i></a>
                </div>
            </div>
            <div class="content-box-large box-with-header">
                
                    {{ csrf_field() }}

                    <div class="form-group{{ $errors->has('fileName') ? ' has-error' : '' }}">
                        <div class="input-group">
                            <div class="input-group-addon">@lang('file.forms.new.textbox_name')</div>
                            <input id="fileName" type="text" class="form-control" name="fileName" value="{{ old('fileName') }}">
                        </div>
                        @if ($errors->has('fileName'))
                            <span class="help-block">
                            <strong>{{ $errors->first('fileName') }}</strong>
                            </span>
                        @endif
                    </div>


                    <div class="form-group{{ $errors->has('filePass') ? ' has-error' : '' }}">
                        <div class="input-group">
                            <div class="input-group-addon">@lang('file.forms.new.textbox_pass')</div>
                            <input id="filePass" type="password" class="form-control" name="filePass" value="{{ old('filePass') }}">
                        </div>
                        @if ($errors->has('filePass'))
                            <span class="help-block">
                            <strong>{{ $errors->first('filePass') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                        <div class="input-group">
                            <div class="input-group-addon">@lang('file.forms.new.textbox_description')</div>
                            <textarea class="form-control" rows="3" id="description" name="description">{{ old('description') }}</textarea>
                        </div>
                        @if ($errors->has('description'))
                            <span class="help-block">
                            <strong>{{ $errors->first('description') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group{{ $errors->has('upFile') ? ' has-error' : '' }}">
                        <div class="input-group">
                            <div class="input-group-addon"></div>
                            <input id="upFile" type="file" class="form-control" name="upFile">
                            <div class="input-group-addon"></div>

                        </div>
                        @if ($errors->has('upFile'))
                            <span class="help-block">
                            <strong>{{ $errors->first('upFile') }}</strong>
                            </span>
                        @endif
                    </div>
            </div>
        </div>

        <div class="col-md-3">
            <div class="content-box-header">
                <div class="panel-title">@lang('file.forms.new.save_box_title')</div>
            </div>
            <div class="content-box-large box-with-header">
                
                <button type="submit" class="btn btn-success btn-lg btn-block" name="opration" value="Upload"><i class="glyphicon glyphicon-saved"></i> @lang('file.forms.new.btn_send') </button>
            </div>
        </div>
    </form>

    </div>
</div>


@endsection

