@extends('layouts.panel')

@section('sidbaractive_dashboard',' class="current"')

@section('content_panel')
          <div class="col-md-10">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="content-box-large">
                        <div class="panel-heading">
                            <div class="panel-title">{{ trans('panel.index.welcome', ['user'=> $username ]) }}</div>
                            
                            <div class="panel-options">
                                <a href="#" data-rel="collapse"><i class="glyphicon glyphicon-question-sign"></i></a>
                                <a href="{{ url('panel/users/edit') }}" data-rel="reload"><i class="glyphicon glyphicon-cog"></i></a>
                            </div>
                        </div>
                        <div class="panel-body">
                            {{ $sett->panel_notic }}
                        </div>
                    </div>
                </div>
            </div>


          </div>
@endsection