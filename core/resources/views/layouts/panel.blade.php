<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Designer" content="AliA_MehR | alia_mehr@yahoo.com">
    <meta name="Generator" content="Sublime Text">
    <meta name="copyright" content="Built-in time 2017-03-04 11:27:50 | home : https://gitlab.com/alia_mehr" />
    <title>@lang('panel.title.site_title')</title>

    <link rel="stylesheet" href="{{ elixir('css/style.css') }}" >
    <link rel="stylesheet" href="{{ elixir('css/admin_style.css') }}" >
    
    <script src="{{ elixir('js/script.js') }}" ></script>  
</head>
<body>
<div class="header">
    <div class="container">
        <div class="row">
            <div class="col-md-5">
              <!-- Logo -->
                <div class="logo">
                    <h1><a href="{{ url('/panel') }}">@lang('panel.title.brand')</a></h1>
                </div>
            </div>

           <div class="col-md-6 pull-right">
                <div class="navbar navbar-inverse" role="banner">
                    <nav class="collapse navbar-collapse bs-navbar-collapse navbar-right" role="navigation">
                        <ul class="nav navbar-nav">
                            <li role="presentation"><a href="{{ url('/panel/') }}" title="@lang('panel.title.nav.dashboard')"><i class="glyphicon glyphicon-dashboard"></i></a></li>
                            <li role="presentation"><a href="{{ url('/') }}" title="@lang('panel.title.nav.site')"><i class="glyphicon glyphicon-home"></i></a></li>
                            <li role="presentation"><a href="{{ url('panel/users/edit') }}" title="@lang('panel.title.nav.profile')"><i class="glyphicon glyphicon-user"></i></a></li>
                            <li role="presentation"><a href="{{ url('/logout') }}" title="@lang('panel.title.nav.exit')"><i class="glyphicon glyphicon-off"></i></a></li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="page-content">
    <div class="row">
      <div class="col-md-2">
        <div class="sidebar content-box" style="display: block;margin-top: -30px;border-radius: 0;">
            <ul class="nav">
                <!-- Main menu -->
                <!-- <li @yield('sidbaractive_dashboard')><a href="{{ url('/panel') }}"><i class="glyphicon glyphicon-home"></i> @lang('panel.navbar.dashboard')</a></li> -->
                @if (session('menu')['Post'][0])
                <li class="submenu @yield('sidbaractive_post')">
                    <a href="#">
                        <i class="glyphicon glyphicon-console"></i>
                        @lang('panel.navbar.posts.list')
                        <span class="caret pull-right"></span>
                    </a>
                    <ul style="display: none;">
                        @if (session('menu')['Post']['all'])
                        <li class="@yield('sidbaractive_allpost')"><a href="{{ url('/panel/posts') }}">@lang('panel.navbar.posts.all')</a></li>
                        @endif

                        @if (session('menu')['Post']['new'])
                        <li class="@yield('sidbaractive_newpost')"><a href="{{ url('/panel/posts/new') }}">@lang('panel.navbar.posts.new')</a></li>
                        @endif

                        @if (session('menu')['Post']['bin'])
                        <li class="@yield('sidbaractive_trashpost')"><a href="{{ url('/panel/posts/trash') }}">@lang('panel.navbar.posts.trash')</a></li>
                        @endif
                    </ul>
                </li>
                @endif

                @if (session('menu')['Page'][0])
                <li class="submenu @yield('sidbaractive_page')">
                    <a href="#">
                        <i class="glyphicon glyphicon-file"></i>
                        @lang('panel.navbar.pages.list')
                        <span class="caret pull-right"></span>
                    </a>
                    <ul style="display: none;">
                        @if (session('menu')['Page']['all'])
                        <li class="@yield('sidbaractive_allpage')"><a href="{{ url('/panel/pages') }}">@lang('panel.navbar.pages.all')</a></li>
                        @endif

                        @if (session('menu')['Page']['new'])
                        <li class="@yield('sidbaractive_newpage')"><a href="{{ url('/panel/pages/new') }}">@lang('panel.navbar.pages.new')</a></li>
                        @endif

                        @if (session('menu')['Page']['bin'])
                        <li class="@yield('sidbaractive_trashpage')"><a href="{{ url('/panel/pages/trash') }}">@lang('panel.navbar.pages.trash')</a></li>
                        @endif
                    </ul>
                </li>
                @endif

                @if (session('menu')['Comments'][0])
                <li class="submenu @yield('sidbaractive_comment')">
                    <a href="#">
                        <i class="glyphicon glyphicon-comment"></i>
                        @lang('panel.navbar.comments.list')
                        <span class="caret pull-right"></span>
                    </a>
                    <ul style="display: none;">
                        @if (session('menu')['Comments']['all'])
                        <li class="@yield('sidbaractive_allcomment')"><a href="{{ url('/panel/comments/me') }}">@lang('panel.navbar.comments.all')</a></li>
                        @endif

                        @if (session('menu')['Comments']['spam'])
                        <li class="@yield('sidbaractive_spamcomment')"><a href="{{ url('/panel/comments/me/spam') }}">@lang('panel.navbar.comments.spam')</a></li>
                        @endif
                    </ul>
                </li>
                @endif

                @if (session('menu')['Seo'][0])
                <li class="submenu @yield('sidbaractive_seo')">
                    <a href="#">
                        <i class="glyphicon glyphicon-screenshot"></i>
                        @lang('panel.navbar.seo.list')
                        <span class="caret pull-right"></span>
                    </a>
                    <ul style="display: none;">
                        @if (session('menu')['Seo']['categories'])<li class="@yield('sidbaractive_allcat')"><a href="{{ url('/panel/seo/categories') }}">@lang('panel.navbar.seo.cats')</a></li>
                        @endif

                        @if (session('menu')['Seo']['tags'])
                        <li class="@yield('sidbaractive_alltag')"><a href="{{ url('/panel/seo/tags') }}">@lang('panel.navbar.seo.tags')</a></li>
                        @endif

                        @if (session('menu')['Seo']['new'])
                        <li class="@yield('sidbaractive_newseo')"><a href="{{ url('/panel/seo/new') }}">@lang('panel.navbar.seo.new')</a></li>
                        @endif
                    </ul>
                </li>
                @endif

                @if (session('menu')['FMS'][0])
                <li class="submenu @yield('sidbaractive_file')">
                    <a href="#">
                        <i class="glyphicon glyphicon-folder-open"></i>
                        @lang('panel.navbar.fms.list')
                        <span class="caret pull-right"></span>
                    </a>
                    <ul style="display: none;">
                        @if (session('menu')['FMS']['all'])
                        <li class="@yield('sidbaractive_allfile')"><a href="{{ url('/panel/files/') }}">@lang('panel.navbar.fms.all')</a></li>
                        @endif

                        @if (session('menu')['FMS']['new'])
                        <li class="@yield('sidbaractive_newfile')"><a href="{{ url('/panel/files/new') }}">@lang('panel.navbar.fms.new')</a></li>
                        @endif

                        @if (session('menu')['FMS']['bin'])
                        <li class="@yield('sidbaractive_trashfile')"><a href="{{ url('/panel/files/trash') }}">@lang('panel.navbar.fms.trash')</a></li>
                        @endif
                    </ul>
                </li>
                @endif

                @if (session('menu')['Users'][0])
                <li class="submenu @yield('sidbaractive_user')">
                    <a href="#">
                        <i class="glyphicon glyphicon-user"></i>
                        @lang('panel.navbar.users.list')
                        <span class="caret pull-right"></span>
                    </a>
                    <ul style="display: none;">
                        <li class="@yield('sidbaractive_actuser')"><a href="{{ url('/panel/users') }}">@lang('panel.navbar.users.active')</a></li>
                        <li class="@yield('sidbaractive_inactuser')"><a href="{{ url('/panel/users/inactive') }}">@lang('panel.navbar.users.deactive')</a></li>
                        <li class="@yield('sidbaractive_newuser')"><a href="{{ url('/panel/users/new') }}">@lang('panel.navbar.users.new')</a></li>
                        <li class="@yield('sidbaractive_edituser')"><a href="{{ url('/panel/users/edit') }}">@lang('panel.navbar.users.edit')</a></li>
                    </ul>
                </li>
                @endif

                @if (session('menu')['Groups'][0])
                <li class="submenu @yield('sidbaractive_groups')">
                    <a href="#">
                        <i class="glyphicon glyphicon-plus-sign"></i>
                        @lang('panel.navbar.groups.list')
                        <span class="caret pull-right"></span>
                    </a>
                    <ul style="display: none;">
                        @if (session('menu')['Groups']['all'])<li class="@yield('sidbaractive_allgroups')"><a href="{{ url('/panel/groups') }}">@lang('panel.navbar.groups.all')</a></li>
                        @endif

                        @if (session('menu')['Groups']['new'])<li class="@yield('sidbaractive_newgroups')"><a href="{{ url('/panel/groups/new') }}">@lang('panel.navbar.groups.new')</a></li>
                        @endif
                    </ul>
                </li>
                @endif

                @if (session('menu')['Settings'][0])
                <li class="@yield('sidbaractive_setting')"><a href="{{ url('/panel/settings') }}"><i class="glyphicon glyphicon-cog"></i> @lang('panel.navbar.settings')</a></li>
                @endif

                @if (session('menu')['Tools'][0])
                <li class="submenu @yield('sidbaractive_tool')">
                    <a href="#">
                        <i class="glyphicon glyphicon-wrench"></i>
                        @lang('panel.navbar.tools.list')
                        <span class="caret pull-right"></span>
                    </a>
                    <ul style="display: none;">
                        @if (session('menu')['Tools'][0])<li class="@yield('sidbaractive_links')"><a href="{{ url('/panel/tools/links') }}">@lang('panel.navbar.tools.link')</a></li>
                        @endif
                    </ul>
                </li>
                @endif

                <!-- <li><a href="{{ url('/logout') }}"><i class="glyphicon glyphicon-off"></i> @lang('panel.navbar.exit')</a></li> -->
            </ul>
        </div>
    </div>
    @yield('content_panel')
</div>
</div>


    <footer class="container-fluid navbar-fixed-bottom">
        <p class="text-center">@lang('panel.footer_text')</p>
    </footer>
<script src="/build/css/js/custom.js"></script>

</body>
</html>