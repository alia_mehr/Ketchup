<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Designer" content="AliA_MehR | alia_mehr@yahoo.com">
    <meta name="Generator" content="Sublime Text">
    <meta name="copyright" content="Built-in time 2017-03-04 11:27:50 | home : https://gitlab.com/alia_mehr" />
    <title>Ketchup</title>
    <link rel="stylesheet" href="{{ elixir('css/style.css') }}" >
    <link rel="stylesheet" href="{{ elixir('css/admin_style.css') }}" >
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>
  <body class="login-bg">
    <div class="header">
       <div class="container">
          <div class="row">
             <div class="col-md-12">
                <!-- Logo -->
                <div class="logo">
                   <h1><a href="index.html">Ketchup Panel</a></h1>
                </div>
             </div>
          </div>
       </div>
  </div>
  @yield('content_auth')
  <link rel="stylesheet" href="{{ elixir('js/script.js') }}" >
  <link rel="stylesheet" href="{{ elixir('js/admin_script.js') }}" >
    
  </body>
</html>