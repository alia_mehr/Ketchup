<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Collection;
class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tempArr = $this->randomArr();
        for ($u=1; $u < 3 ; $u++) { 
        	for ($i=0; $i < 11 ; $i++) { 
        		DB::table('posts')->insert([
		            'user_id' => $u,
		            'title' => $tempArr['titleArr'][(round($i%3)+1)],
		            'body' => $tempArr['bodyArr'][(round($i%3)+1)],
		            'status' => rand(0,5),
		            'comments_status' => rand(0,2),
		            'created_at' => "2017-03-".rand(0,2)."6 15:".rand(10,59).":53",
		            'updated_at' => "2017-03-".rand(0,2)."6 15:".rand(10,59).":53",
	        	]);
        	}
        }
    }
    private function randomArr(){
    	$temp = 'Lorem ipsum Deserunt Duis commodo id ullamco minim id sed qui Duis do sed officia amet anim dolor nulla nulla culpa dolor reprehenderit laborum fugiat nostrud laborum eiusmod elit cupidatat commodo incididunt nulla magna ad esse anim exercitation dolore esse anim nostrud fugiat ullamco incididunt consectetur voluptate amet in in cupidatat magna veniam cupidatat sunt et sint consectetur fugiat ad anim id dolore magna est in aute adipisicing consectetur esse magna sit consectetur laborum nostrud cillum exercitation magna nulla adipisicing ut labore labore id magna exercitation ullamco do do dolor qui dolor sit elit anim aute et irure fugiat veniam aliqua non commodo et qui enim cupidatat ut tempor elit velit mollit Ut tempor aute aliqua velit Duis id occaecat veniam mollit esse dolor eu minim in in ullamco ullamco ut irure nisi ex ullamco est laboris incididunt ullamco in laboris cupidatat officia qui Duis in minim velit mollit nostrud ad irure dolore id elit eiusmod eiusmod voluptate enim in enim Excepteur culpa anim elit adLorem ipsum Minim tempor mollit voluptate voluptate Excepteur incididunt reprehenderit occaecat incididunt veniam aute ea cillum eu aliqua tempor Duis labore in dolor non consectetur sint ex sed cupidatat pariatur aliqua occaecat consequat laborum in eu Ut in ut id ut nisi esse ullamco culpa incididunt adipisicing dolore occaecat enim nostrud ex adipisicing eu consequat ea dolore ea minim non officia eu laboris esse est sint incididunt sit veniam ullamco in mollit ut cupidatat ut deserunt id voluptate ex id do consequat et commodo officia nostrud in veniam culpa in cupidatat nisi dolor nostrud irure elit mollit consequat minim ea Duis dolor in Ut dolor nulla occaecat dolor quis veniam ut quis Ut labore labore consectetur labore occaecat in Excepteur consequat tempor in ut aliqua laborum elit amet proident exercitation aliquip Duis laborum minim ullamco irure do in aliquip dolore enim occaecat cupidatat ea eiusmod nostrud reprehenderit ut aute ullamco aliquip velit minim nulla proident commodo ut ex sint Excepteur in ex ut nisi consectetur irure anim elit culpa mollit consectetur tempor.ipisicing id ut nisi consequat consectetur laboris officia laborum labore cupidatat et officia consequat sit commodo velit Ut elit dolor irure ad id fugiat elit proident sint do do pariatur consequat dolor ad Excepteur amet in pariatur consectetur proident voluptate est dolore ea proident anim occaecat Excepteur dolor veniam amet sunt Duis laborum enim elit.
        	Lorem ipsum Cillum proident et magna amet ex in enim consequat Excepteur fugiat ad sed magna labore reprehenderit ad id consequat occaecat enim est incididunt est ex incididunt qui aliqua voluptate nisi ex enim in deserunt commodo in commodo laboris id cupidatat eu culpa in nulla cillum id Duis in magna ea tempor laborum ut ut occaecat cupidatat est dolore irure officia elit ut sed tempor in exercitation aliquip irure esse sed officia elit irure deserunt Excepteur non sed enim sed mollit voluptate irure est do dolor qui adipisicing aute sint deserunt exercitation et eu enim Excepteur incididunt Ut quis amet consectetur consequat exercitation non in ut nisi laboLorem ipsum Laboris dolor officia nisi consequat ad et pariatur ex adipisicing ut magna culpa veniam dolor aliqua laboris Ut voluptate voluptate incididunt in aute id Excepteur fugiat laborum Duis quis veniam proident minim velit consequat in voluptate dolor sint laborum commodo ut laborum voluptate dolore reprehenderit sit minim reprehenderit dolor mollit pariatur pariatur quis adipisicing incididunt sit eiusmod in amet minim occaecat nulla sed aliqua labore irure elit tempor consequat enim dolor officia commodo veniam deserunt eiusmod incididunt ullamco do sint eiusmod quis proident enim pariatur dolor cillum in ea ea non mollit deserunt ut anim dolore exercitation aliqua irure nulla Excepteur qui sed nostrud deserunt exercitation magna in aliqua elit aliquip mollit mollit nulla sunt ea cupidatat ea sunt incididunt officia labore aute commodo fugiat et Duis velit dolore esse nulla commodo labore exercitation labore mollit esse magna exercitation est eiusmod ut amet esse consectetur in nulla nisi proident fugiat occaecat do aliqua laborum dolor.re sunt eu deserunt ex laboris Ut quis Excepteur reprehenderit aute dolor sed minim exercitation qui nulla mollit et cupidatat nostrud culpa quis dolor adipisicing laboris quis cillum non occaecat nulla Excepteur aute esse Duis esse in labore deserunt id minim in labore laboris voluptate aliqua ut et dolore dolor irure officia nisi in sed magna mollit reprehenderit sit amet incididunt Ut dolor consequat et cillum elit ut labore Ut deserunt dolor sint dolor aute quis in adipisicing laborum fugiat proident irure ad labore fugiat veniam proident commodo in anim et ut velit elit deserunt nisi adipisicing reprehenderit occaecat reprehenderit labore ad est consectetur consequat elit anim laborum dolore officia sed eu do et tempor nulla in incididunt dolor sit sint enim Ut in esse fugiat mollit sunt mollit nulla ullamco aliquip labore officia ut pariatur incididunt occaecat amet ad dolor minim dolor Ut tempor adipisicing Ut consequat esse Duis dolore do ex mollit.';
        $titleArr = [];
        $bodyArr = [];
        for ($a=1; $a < 5; $a++) { 
        	$arrPointer1 = $a*($a+2);
        	$arrPointer2 = $a*($a+4);
        	$titleArr[] = substr($temp,$arrPointer2, $arrPointer1 + 10);
        	$bodyArr[] = substr($temp,$arrPointer1, $arrPointer2 + 255*$arrPointer1);
        }
        $arr['titleArr']= $titleArr;
        $arr['bodyArr']= $bodyArr;
        return $arr;
    }
}
