<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInformationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('information', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('name')->unique();
            $table->string('picture',1000)->nullable();
            $table->text('about')->nullable();
            $table->dateTime('brithTime')->nullable();
            $table->string('education')->nullable();
            $table->string('job',100)->nullable();
            $table->string('favorites',500)->nullable();
            $table->string('email')->nullable();
            $table->string('phone',30)->nullable();
            $table->string('address',2000)->nullable();
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('information');
    }
}
