<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\User;

class File extends Model
{
    use SoftDeletes;
	
    public function user(){
        return $this->belongsTo('App\User');
    }
}
