<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\CreatePostRequest;
use App\Post;
use App\User;
use App\Category;
use App\Tag;
use Auth;

class PostController extends Controller
{
    public function listPosts($from = 'me'){
        if ($this->uAr[3] || $this->mAr[2]) {
            switch ($from) {
                case 'me':
                    if(!$this->uAr[3]){
                        return redirect('panel/posts/all');
                    }
                    $allPosts = Auth::user()->posts()->where('url',null)->get();
                break;
                
                case 'all':
                    if(!$this->mAr[2]){
                        return redirect('panel/posts');
                    }
                    $allPosts = Post::where('url',null)->get();
                break;
            }
        } else {
            return redirect('panel');
        }
        return view('panel.posts.index',compact('allPosts','from'));
    }

    public function newPost(){
        if (!$this->uAr[0]) {
            return redirect('panel');
        }
        $allCategories = Category::all()->toArray();
        return view('panel.posts.newpost',compact('allCategories')); 
    }

    public function store(CreatePostRequest $r){
        if (!$this->uAr[0]) {
            return redirect('panel');
        }
        $data['url'] = null;                                //for page posts
        $data['title'] = $r->input('postTitle');
        $data['body'] = $r->input('postContent');
        $data['status'] = $r->input('statusPost');
        $data['comments_status'] = $r->input('statusComment');
        $newpost = Auth::user()->posts()->create($data);
        if(empty($r->input('category'))){
             $tempCat = [0 => '1'];
        }
        $newpost->cats()->sync($r->input('category'));
        if (strlen(trim($r->input('postTags'))) > 0) {
            $data['postTags'] = $this->setTags($r->input('postTags')) ;
            if ($data['postTags'] != null) {
                $newpost->tags()->sync($data['postTags']);
            }
        }
        return redirect('panel/posts');
    }
 
    public function edit(Post $post){    
        if($post->user_id == Auth::user()->id){
            if (!$this->uAr[1]) {
                return redirect('panel');
            }
        } else {
            if (!$this->mAr[0]) {
                return redirect('panel');
            }
        }
        if ($post->url === null) {
            $allCategories = Category::all()->toArray();
            return view('panel.posts.edit',compact(['allCategories','post']));
        } else {
            return redirect('panel/posts');
        }    
    }

    public function postUpdate(CreatePostRequest $r,$id){
        $upPost = Post::find($id);
        if($upPost->user_id == Auth::user()->id){
            if (!$this->uAr[1]) {
                return redirect('panel');
            }
        } else {
            if (!$this->mAr[0]) {
                return redirect('panel');
            }
        }
        $data['url'] = null;                                //for page posts
        $data['title'] = $r->input('postTitle');
        $data['body'] = $r->input('postContent');
        $data['status'] = $r->input('statusPost');
        $data['comments_status'] = $r->input('statusComment');
        $upPost->update($data);
        if(empty($r->input('category'))){                    // should TEST
             $tempCat = [0 => '1'];
        }
        $upPost->cats()->sync($r->input('category'));
        if (strlen(trim($r->input('postTags'))) > 0) {
            $data['postTags'] = $this->setTags($r->input('postTags')) ;
            if ($data['postTags'] != null) {
                $upPost->tags()->sync($data['postTags']);
            }
        }
        return redirect('panel/posts');
    }

    public function deletePost(Post $post){   // soft delete
        if($post->user_id == Auth::user()->id){
            if (!$this->uAr[2]) {
                return redirect('panel');
            }
        } else {
            if (!$this->mAr[1]) {
                return redirect('panel');
            }
        }

        if ($post->url === null) {
            $post->delete();
        }
        return redirect('panel/posts');
    }

    protected function setTags($allTags){
        $tagsArray = (strpos($allTags, '+') != false) ? explode('+', $allTags) : [ 0 => $allTags];
        $tempTag = new Tag;
        foreach ($tagsArray as $tag) {
            $tempIdTag = $tempTag->select('id')->where('name', $tag)->first();
            if ($tempIdTag != null) {
                if (($this->uAr[14]) || ($this->mAr[11])) {
                    $tempIdTag = $tempIdTag->toArray();
                } else {
                    continue;
                }
            } else {
                if (($this->uAr[12]) || ($this->mAr[9])) {
                    $tempIdTag = $tempTag->create(['name' => $tag])->toArray();
                } else {
                    continue;
                }
            }
            $idTag[] = $tempIdTag['id'];
        }
        return $idTag;
    }

    public function trashBin($from = 'me'){
        if (($this->uAr[2] && $this->uAr[3]) || ($this->mAr[1] && $this->mAr[2])) {
            switch ($from) {
                case 'me':
                    if(!($this->uAr[2] && $this->uAr[3])){
                        return redirect('/panel/posts/trash/all');
                    }
                    $allPosts = Auth::user()->posts()->onlyTrashed()->where('url',null)->get();
                break;
                
                case 'all':
                    if(!($this->mAr[1] && $this->mAr[2])){
                        return redirect('/panel/posts/trash/');
                    }
                    $allPosts = Post::onlyTrashed()->where('url',null)->get();
                break;
            }
        } else {
            return redirect('panel');
        }
        return view('panel.posts.trashbin',compact('allPosts','from'));
    }

    public function restorPost($id){
        $resPost = Post::onlyTrashed()->where('url',null)->find($id);

        if($resPost->user_id == Auth::user()->id){
            if (!$this->uAr[2]) {
                return redirect('panel');
            }
        } else {
            if (!$this->mAr[1]) {
                return redirect('panel');
            }
        }
        $resPost->restore();
        return redirect('panel/posts/trash');
    }
    public function removePost($id){
        $delPost = Post::onlyTrashed()->where('url',null)->find($id);
        if($delPost->user_id == Auth::user()->id){
            if (!$this->uAr[2]) {
                return redirect('panel');
            }
        } else {
            if (!$this->mAr[1]) {
                return redirect('panel');
            }
        }
        $delPost->forceDelete();
        return redirect('panel/posts/trash');
    }
}
