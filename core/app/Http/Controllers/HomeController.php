<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\SettingController;
use Auth;
use App\Post;
use App\Link;
use App\User;
use App\Comment;
use Carbon\Carbon;
class HomeController extends Controller
{
    private $init;
    private $temp = [];
    public function __construct(){
        $this->init = SettingController::get();
        $this->siteAvailability();
    }

    public function index(Request $r){
        if ($r->search == null) {
            $posts = Post::where('status','<','2')->whereNull('url')->orderBy('updated_at', 'desc')->paginate($this->init->total_post);
        } else {
            $posts = Post::where('status','<','2')->where('body', 'LIKE', "%$r->search%")->orderBy('updated_at', 'desc')->get();
        }
        foreach ($posts as $post) {
            if ((Auth::guest()) && ($post->status == 1)) {
                $post->body = trans('home.errors.user_posts');
            }
        }
        $links = Link::get();
        $this->init->user_id = User::find($this->init->user_id)->name;
        $init = $this->init;
    	return view('home.recentpost',compact('init','posts','links'));
    }

    public function filterPosts(Request $r){
        switch (array_keys($r->all())[0]) {
            case 'author':
                $posts = User::where('name',$r->author)->first()->posts()->where('status','<','2')->orderBy('updated_at', 'desc')->paginate($this->init->total_post);
            break;
            case 'date':
                $posts = Post::where([['updated_at', $r->date],['status','<','2']])->orderBy('updated_at', 'desc')->paginate($this->init->total_post);
            break;
            case 'category':
                $posts = Post::where('status','<','2')->orderBy('updated_at', 'desc')->paginate($this->init->total_post);
                foreach ($posts as $key => $post) {
                    $flag = false;
                    foreach ($post->catNames as $cat) {
                        if ($cat == $r->category) {
                            $flag = true;
                             break;
                        }
                    }
                    if (!$flag) {
                        unset($posts[$key]);
                    }
                }
            break;
            case 'tag':
                $posts = Post::where('status','<','2')->orderBy('updated_at', 'desc')->paginate($this->init->total_post);
                foreach ($posts as $key => $post) {
                    $flag = false;
                    foreach ($post->tagName as $tag) {
                        if ($tag == $r->tag) {
                            $flag = true;
                             break;
                        }
                    }
                    if (!$flag) {
                        unset($posts[$key]);
                    }
                }
            break;
            default:
                return redirect('/');
            break;
        }
        foreach ($posts as $post) {
            if ((Auth::guest()) && ($post->status == 3)) {
                $post->body = trans('home.errors.user_posts');
            }
        }
        $links = Link::get();
        $this->init->user_id = User::find($this->init->user_id)->name;
        $init = $this->init;
        return view('home.recentpost',compact('init','posts','links'));
    }

    public function profile($user){
        $user = User::where('name',$user)->first();
        $info = $user->information;
        $posts = $user->posts()->where('status','<','2')->orderBy('updated_at', 'desc')->paginate($this->init->total_post);
        $init = $this->init;
        return view('home.author',compact('init','user','info','posts'));
    }

    public function showPage($url){
        $post = Post::where('url',$url)->first();
        if (Auth::guest()) {
            if($post->status == 1) {
                $post->body = trans('home.errors.user_posts');
            }
        } else {
            $user = Auth::user();
        }

        if($post->status > 1) {
            $err['title'] = trans('home.errors.404.title');
            $err['body'] = trans('home.errors.404.body');
            return view('errors.index',compact('err')) ;
        }

        if (isset($r->cmid)) {
            $commFlag = Comment::find($r->cmid);
            $commFlag = trans('home.index.comment_reply_title').$commFlag->name;
        }
        
        $comms = $post->comments()->where('parent', null)->where('status', 1)->orWhere('status',3)->orderBy('created_at', 'asc')->get();
        $commArr = [];
        foreach ($comms as $c) {
            $indexName = $c->id . 'b';
            if (count($c->sons)<1) {
                $indexName .= '0';
                $commArr[$indexName] = $c;
            } else {
                $commArr[$indexName] = $c;
                $commArr = array_merge($this->sCheck($c->sons,$c,$commArr),$commArr);
            }
        }
        $comms = $commArr;
        $commArr = [];
        foreach ($comms as $key => $c) {
            if (($c->status == 1) || ($c->status == 3)) {
                $commArr[$key] = $c;
            }
        }
        $links = Link::get();
        $comms = $commArr;
        $init = $this->init;
        return view('home.post',compact('init','post','comms','user','commFlag','links'));

    }

    public function showPost(Request $r, Post $post){
        if (Auth::guest()) {
            if($post->status == 1) {
                $post->body = trans('home.errors.user_posts');
            }
        } else {
            $user = Auth::user();
        }
        if($post->status > 1) {
            $err['title'] = trans('home.errors.404.title');
            $err['body'] = trans('home.errors.404.body');
            return view('errors.index',compact('err')) ;
        }
        
        if (isset($r->cmid)) {
            $commFlag = Comment::find($r->cmid);
            $commFlag = trans('home.index.comment_reply_title').$commFlag->name;
        }
        $comms = $post->comments()->where('parent', null)->where('status', 1)->orWhere('status',3)->orderBy('created_at', 'asc')->get();
        $commArr = [];
        foreach ($comms as $c) {
            $indexName = $c->id . 'b';
            if (count($c->sons)<1) {
                $indexName .= '0';
                $commArr[$indexName] = $c;
            } else {
                $commArr[$indexName] = $c;
                $commArr = array_merge($this->sCheck($c->sons,$c,$commArr),$commArr);
            }
        }
        $comms = $commArr;
        $commArr = [];
        foreach ($comms as $key => $c) {
            if (($c->status == 1) || ($c->status == 3)) {
                $commArr[$key] = $c;
            }
        }
        $comms = $commArr;
        $links = Link::get();
        $init = $this->init;
        return view('home.post',compact('init','post','comms','user','commFlag','links'));
    }

    private function sCheck($comments,$father,$commArr =[]){
        foreach ($comments as $s) {
            $indexName = $s->id . 'b' . $father->id;
            if (count($s->sons)<1) {
                $commArr[$indexName] = $s;
            } else {
                $commArr[$indexName] = $s;
                $commArr = array_merge($this->sCheck($s->sons,$s,$commArr),$commArr);
            }
        }
        return $commArr;
    }

    public function createComment(Request $r, $post){
        $tempTime = Carbon::now();
        $this->validate($r,[
            'commName' => 'required|string|between:1,30',
            'commEmail' => 'required|email',
            'commBody' => 'required|string|min:10',
            '_token' => 'required|alpha_dash'
        ]);
        $pComm = null;
        if (isset($r->cmid)) {
            $pComm = Comment::find($r->cmid);           // parent Comment
        }
        $newComm = new Comment;
        $newComm->post_id = $post->id;
        $newComm->name = $r->commName;
        $newComm->email = $r->commEmail;
        $newComm->body = $r->commBody;
        $newComm->parent = ($pComm != null) ? $r->cmid : null;
        $newComm->status = 0;
        $newComm->csrf = $r->_token;
        $newComm->created_at = $tempTime;
        $newComm->save();
        if ($pComm != null) {
            $pComm->status = 3;
            $pComm->save();
        }
        return redirect(url()->current());
    }

    protected function siteAvailability(){
        switch ($this->init->availability) {
            case 'all':
            break;
            case 'users':
                if (Auth::guest()) {
                    die(redirect()->guest('login'));
                }
            break;
            case 'nobody':
                if ((Auth::guest()) || (Auth::user()->id != $this->init->user_id)) {
                    $body = $this->init->site_notic;
                    die(view('errors.nobody',compact('body'))) ;

                }
            break;
        }
    }
}
